﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace isphone1
{
    class Program
    {
        public static class Data
        {
            public static string[] arr = new string[8];
        }
        static bool IsPhone(string s)
        {
            Console.WriteLine(s);
            return Regex.IsMatch(s, @"^\(?\d{3}\)?[\s\-]?\d{3}\-?\d{4}$");
        }
        static bool IsZip(string s)
        {
            return Regex.IsMatch(s, @"^\d{5}(\-\d{4})?$");
        }


        static void Main(string[] args)
        {
            for (int i = 1; i <= 8; i++)
            {
                Console.Write(Data.arr[i]);
                Data.arr[i] = Console.ReadLine();

                {
                    if (IsPhone(Data.arr[i]))
                    {
                        Console.WriteLine(Data.arr[i] + " is a phone number");
                    }
                    else if (IsZip(Data.arr[i]))
                    {
                        Console.WriteLine(Data.arr[i] + " is a zip code");
                    }
                    else
                        Console.WriteLine(Data.arr[i] + " is unknown");
                }


                Console.ReadKey();
            }
        }
    }
}

